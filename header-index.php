<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>OKIF FT-UH</title>
	<meta charset="UTF-8">
	<meta name="description" content="Organisasi Kemahasiswaan Informatika Fakultas Teknik Universitas Hasanuddin">
	<meta name="keywords" content="Organisasi Kemahasiswaan Informatika Fakultas Teknik Universitas Hasanuddin">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="HTML/img/iconif.png" rel="icon" type="image/png"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">


	<!-- Stylesheets -->
	<link rel="stylesheet" href="HTML/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="HTML/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="HTML/css/slicknav.min.css"/>
	<link rel="stylesheet" href="HTML/css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="HTML/css/magnific-popup.css"/>
	<link rel="stylesheet" href="HTML/css/animate.css"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="HTML/css/style.css"/>
	
	<!-- NEW STYLE -->
	<link rel="stylesheet" href="HTML/css/new_style.css"/>
	<link rel="stylesheet" href="HTML/css/style_baru.css"/>


	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section">
		<div class="header-warp">
			<div class="header-bar-warp d-flex">
				<!-- site logo -->
				<a href="HTML/index.php" class="site-logo">
					<img src="HTML/img/if.png" alt="logo" class="logo">
				</a>
				<nav class="top-nav-area w-100">
					<!-- Menu -->
					<ul class="main-menu primary-menu navigationList">
						<li><a href="HTML/index.php">Home</a></li>
						<li><a href="HTML/about.php">About</a></li>
						<li class="dropdown"><a href="#" >Pengurus</a>
							<ul class="sub-menu">
								<li><a href="HTML/hmif.php">HMIF FT-UH</a></li>
								<li><a href="HTML/dmmif.php">DMMIF FT-UH</a></li>
							</ul>
						</li>
						<li><a href="HTML/blog.php">Blog</a></li>
						<li><a href="HTML/prestasi.php"> Daftar Prestasi</a></li>
						<li><a href="HTML/contact.php">Contact Us</a></li>
						<li>
							<div class="widget-item" role="search" action=HTML/pencarian.php?cari-akun>
								<form class="search-widget col-md-9 col-lg-9" role="search" action=HTML/pencarian.php?cari-akun>
									<input type="text" name="judul" placeholder="Cari Artikel" class="inputph">
									<button type="submit"><div style="color:#fff;">Cari</div></button>
								</form>
							</div>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- Header section end -->