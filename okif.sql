-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2019 at 09:08 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `okif`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(5) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap_admin` varchar(100) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `foto` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama_lengkap_admin`, `last_login`, `status`, `foto`) VALUES
(1, 'ikbal', 'ikbal', 'ikbal koboy junior', NULL, NULL, 'sadasd');

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `judul` varchar(75) NOT NULL,
  `isi` text NOT NULL,
  `foto` varchar(75) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `penulis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id`, `judul`, `isi`, `foto`, `kategori`, `tanggal`, `penulis`) VALUES
(7, 'adadsa', '<p>aaaaaaaaaaaaaaa</p>', 'basic.PNG_1561760588.png', 'info kegiatan', '2019-06-03', 'aaaaaaa'),
(8, 'sdasdasdads', '<p>asdsadasdads</p>', '596af51528056b7c7a9ac0e0_l_1561760568.jpg', 'info kegiatan', '2019-06-13', 'asdads'),
(9, 'sadadadas', '<p>asdasdasd</p>', '596af51528056b7c7a9ac0e0_l_1561773763.jpg', 'info beasiswa', '2019-06-18', 'asdasd'),
(10, 'sadadasdasd', '<p>asdsadaads</p>', '596af51528056b7c7a9ac0e0_l_1561773778.jpg', 'info beasiswa', '2019-06-13', 'asdasd'),
(11, 'asdadads', '<p>asdadasd</p>', 'hgkkjh (2).PNG_1561773792.png', 'info kemahasiswaan', '2019-06-17', 'asddsa'),
(12, 'asdadasd', '<p>asdadsada</p>', 'basic.PNG_1561773808.png', 'prestasi', '2019-06-08', 'asdad');

-- --------------------------------------------------------

--
-- Table structure for table `dmmif`
--

CREATE TABLE `dmmif` (
  `id` int(11) NOT NULL,
  `nama` varchar(75) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `periode` varchar(75) NOT NULL,
  `foto` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dmmif`
--

INSERT INTO `dmmif` (`id`, `nama`, `jabatan`, `periode`, `foto`) VALUES
(8, 'hhiljlkj', 'Ketua Dewan', 'Masa Bakti 2018/2019', '596af51528056b7c7a9ac0e0_l_1561745025.jpg'),
(9, 'hhiljlkj', 'Sekretaris Dewan', 'Masa Bakti 2018/2019', 'Capture.PNG_1561745036.png'),
(11, 'sasd', 'Anggota', 'Masa Bakti 2018/2019', 'hgkkjh (2).PNG'),
(12, 'wew', 'Anggota', 'Masa Bakti 2018/2019', 'gkjgkj.PNG'),
(13, 'hhiljlkj', 'Anggota', 'Masa Bakti 2018/2019', '596af51528056b7c7a9ac0e0_l_1561746042.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `fungsi`
--

CREATE TABLE `fungsi` (
  `id` int(11) NOT NULL,
  `fungsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hmif`
--

CREATE TABLE `hmif` (
  `id` int(11) NOT NULL,
  `nama` varchar(75) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `periode` varchar(40) NOT NULL,
  `foto` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hmif`
--

INSERT INTO `hmif` (`id`, `nama`, `jabatan`, `periode`, `foto`) VALUES
(10, 'hhiljlkj', 'Ketua Umum', 'Masa Bakti 2018/2019', '596af51528056b7c7a9ac0e0_l.jpg'),
(11, 'asdasd', 'Wakil Ketua Umum', 'Masa Bakti 2018/2019', 'gkjgkj.PNG'),
(12, 'weqesad', 'Sekretaris Umum', 'Masa Bakti 2018/2019', 'dghf.PNG'),
(13, 'sasd', 'Bendahara Umum', 'Masa Bakti 2018/2019', 'Capture.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `ketentuan`
--

CREATE TABLE `ketentuan` (
  `id` int(11) NOT NULL,
  `ketentuan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(75) NOT NULL,
  `prestasi` varchar(100) NOT NULL,
  `kegiatan` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

CREATE TABLE `saran` (
  `id_saran` int(5) NOT NULL,
  `saran_nama` varchar(50) NOT NULL,
  `saran_email` varchar(50) NOT NULL,
  `saran_perihal` varchar(200) NOT NULL,
  `saran_isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id_saran`, `saran_nama`, `saran_email`, `saran_perihal`, `saran_isi`) VALUES
(1, 'Hermawan Safrin', 'var_email', 'var_perihal', 'Lorem Ipsum Dolor Sit Amet'),
(2, 'Gilbert', 'var_email', 'var_perihal', 'dlasdklas;dksal;dkasl;dkas asd;aslk das;kd ;lasdk ;asldk l;asdksa;ldasl; dkas;ldk sa;'),
(3, 'Jumel', 'jumel@gmail.com', 'var_perihal', 'askldsakldasd aslkdnasld asdlk asdl asdlk sadsa dlkas dlkas daskl dlask daskld askldasdklas dsa dkasdmaslkdmas kldmas kldmas kdlasm dsa mdklasm dlk asmd adl aksmdasl kdmadl ma dm al'),
(4, 'Sanji', 'Sanji09@gmail.com', 'asdas', 'ekasmdasklmdasklmdkaslmdkl'),
(5, 'Sanji', 'Sanji09@gmail.com', 'asdas', 'ekasmdasklmdasklmdkaslmdkl'),
(6, 'Sanji', 'Sanji09@gmail.com', 'asdas', 'ekasmdasklmdasklmdkaslmdkl'),
(7, 'dsad', 'dasdas@gmail.cmo', 'sadas@gmail.com', 'kasmdasdsamd.,mas.,dmas.,dmas,dmas.,dmas.dmsd asdm as,dmasdasndmsand'),
(8, 'asdasd', 'asdasd@gmail.com', 'asdasd', 'asdasdaczxcxzcasdwqdwadsa');

-- --------------------------------------------------------

--
-- Table structure for table `sejarah`
--

CREATE TABLE `sejarah` (
  `id` int(11) NOT NULL,
  `sejarah` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sejarah`
--

INSERT INTO `sejarah` (`id`, `sejarah`) VALUES
(1, '<p>adsda</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tentang`
--

CREATE TABLE `tentang` (
  `id` int(11) NOT NULL,
  `sejarah` text NOT NULL,
  `ketentuan` text NOT NULL,
  `tujuan` text NOT NULL,
  `fungsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tujuan`
--

CREATE TABLE `tujuan` (
  `id` int(11) NOT NULL,
  `tujuan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dmmif`
--
ALTER TABLE `dmmif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fungsi`
--
ALTER TABLE `fungsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hmif`
--
ALTER TABLE `hmif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ketentuan`
--
ALTER TABLE `ketentuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id_saran`);

--
-- Indexes for table `sejarah`
--
ALTER TABLE `sejarah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tentang`
--
ALTER TABLE `tentang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tujuan`
--
ALTER TABLE `tujuan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `dmmif`
--
ALTER TABLE `dmmif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `fungsi`
--
ALTER TABLE `fungsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hmif`
--
ALTER TABLE `hmif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `ketentuan`
--
ALTER TABLE `ketentuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id_saran` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sejarah`
--
ALTER TABLE `sejarah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tentang`
--
ALTER TABLE `tentang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tujuan`
--
ALTER TABLE `tujuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
