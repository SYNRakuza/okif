<?php include('component/header.php'); ?>

<?php
function limit_words($string, $word_limit){
$words = explode(" ",$string);
return implode(" ", array_splice($words,0,$word_limit));
}
?>

	<!-- Hero section -->

	<section class="hero-section overflow-hidden">
		<div class="hero-slider owl-carousel">
			<div class="hero-item set-bg d-flex align-items-center justify-content-center text-center firstLandingPage" data-setbg="img/HMIF.png">
				<div class="container">
					<h1 class="newH1"> <span class="span" style="font-size: 12vw;">OKIF </span><span style="font-size:12vw;">FT-UH</span></h1>
					<!-- <a href="#" class="site-btn">Read More  <img src="img/icons/double-arrow.png" alt="#"/></a> -->
				</div>
			</div>
			<div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="img/background.png"\>
				<div class="container">
					<h1 class="newH1"> <span class="span" style="font-size: 12vw;">OKIF </span><span style="font-size: 12vw;">FT-UH</span></h1>
					<!-- <a href="#" class="site-btn">Read More  <img src="img/icons/double-arrow.png" alt="#"/></a> -->
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end-->

	<!-- Intro section -->
	<section class="intro-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="intro-text-box text-box text-white">
						<h2 class="newH2"><span style="font-size: 12vw;">WELCOME!</span></h2>
						<p>Organisasi Kemahasiswaan Informatika Fakultas Teknik Universitas Hasanuddin ialah Organisasi Kemahasiswaan di Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin yang kemudian disingkat OKIF FT-UH</p>
						<a href="about.php" class="read-more">More <span class="span">About Us! </span> <img src="img/icons/double-arrow.png" alt="#"/></a>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	<!-- ketuan lembaga -->
	<section class="review-section" style="align: center;">
		<div class="container">
				<div class="row">
				<?php include 'koneksi.php'  ?>

				<div class="col-sm-6">
					<?php 
					$data = mysqli_query($koneksi,"SELECT * FROM dmmif");
					$get = mysqli_fetch_array($data)
					?>
						<a href="dmmif.php">
						<div class="card mb-3" style="max-width: 560px;">
						<div class="row no-gutters">
							<div class="col-md-4">
							<span style="max-width:50%; height: auto;"><?="<img src='admin/image/".$get['foto']."''>"?></span>
							</div>
							<div class="col-md-8">
							<div class="card-body">
								<h5 class="card-title"> Ketua DMMIF FT-UH <br><?= $get['periode']?></h5>
								<p class="card-text"><?= $get['nama']?></p>
							</div>
							</div>
						</div>
						</div>
						</a>
				</div>
				<div class="col-sm-6">
					<?php 
					$data = mysqli_query($koneksi,"SELECT * FROM hmif");
					$get = mysqli_fetch_array($data)
					?>
						<a href="hmif.php">
						<div class="card mb-3" style="max-width: 560px;">
						<div class="row no-gutters">
							<div class="col-md-4">
							<span style="max-width:50%; height: auto;"><?="<img src='admin/image/".$get['foto']."''>"?></span>
							</div>
							<div class="col-md-8">
							<div class="card-body">
								<h5 class="card-title">Ketua HMIF FT-UH <br><?= $get['periode']?></h5>
								<p class="card-text"><?= $get['nama']?></p>
							</div>
							</div>
						</div>
						</div>
						</a>
				</div>
				</div>

		</div>
	</section>



	<!-- Blog section -->
	<section class="blog-section spad">
		<div class="container">
			<div class="row">
				<div class="col-xl-9 col-lg-8 col-md-7">
					<div class="section-title text-white">
						<h2>Latest Blog</h2>
					</div>
					<!-- Blog item -->
					<?php include 'koneksi.php'  ?>
					<?php 
						$data = mysqli_query($koneksi, "SELECT * FROM artikel  ORDER BY id DESC LIMIT 3");
						while ($get = mysqli_fetch_array($data)) {
							$waktu = date('d M Y', strtotime($get['tanggal']));
					?>
					<div class="blog-item">
						<div class="blog-thumb">
							<?= "<img src='admin/image/" . $get['foto'] . "''>" ?>
						</div>
						<div class="blog-text text-box text-white">
							<div class="top-meta"><?= $waktu ?>  /  Kategori <a><?= $get['kategori'] ?></a></div>
							<h3><?= $get['judul'] ?></h3>
							<?php
							$long_string = $get['isi'];
							$limited_string = limit_words($long_string, 50);
							echo $limited_string."...";
							?>
							<a href="readmore.php?id=<?php echo $get['id']; ?>" class="read-more">Read More <img src="img/icons/double-arrow.png" alt="readmore.php" /></a>
						</div>
					</div>
					<?php } ?>
					<!-- Blog item -->
				</div>
				<div class="col-xl-3 col-lg-4 col-md-5 sidebar">
					<div id="stickySidebar">
						<div class="widget-item">
							<div class="categories-widget">
								<h4 class="widget-title">Kategori</h4>
								<ul>
								<li><a href="jenis_artikel.php?kategori=info%20kegiatan">Info Kegiatan</a></li>
								<li><a href="jenis_artikel.php?kategori=info%20beasiswa">Info Beasiswa</a></li>
								<li><a href="jenis_artikel.php?kategori=prestasi">Prestasi</a></li>
								<li><a href="jenis_artikel.php?kategori=info%20kemahasiswaan">Informasi Kemahasiswaan</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog section end -->

	<!-- Newsletter section -->
	<!-- <section class="newsletter-section">
		<div class="container">
			<h2>INTERESTED WITH US?</h2>
			<p class="footer">Untuk informasi lebih lanjut, hubungi kami dengan menekan tombol dibawah</p>
			<button class="myButton"> <a href="">Contact Us</a></button>
		</div>
	</section> -->
	<!-- Newsletter section end -->


	<?php include('component/footer.php'); ?>