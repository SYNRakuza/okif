<?php include('component/header.php'); ?>

	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg/6.jpg">
		<div class="page-info">
			<h2>Blog</h2>
			<div class="site-breadcrumb">
				<a href="">Home</a>  /
				<span>Blog</span>
			</div>
		</div>
	</section>
    <!-- Page top end-->
    
    <!-- Blog page -->
	<section class="games-single-page">
		<div class="container">
			<div class="row">
				<div class="col-xl-9 col-lg-8 col-md-7 game-single-content">
					<?php include 'koneksi.php'  ?>
					<?php 
                        $id=$_GET['id'];
						$data = mysqli_query($koneksi,"SELECT * FROM artikel WHERE id='$id'");
						$get = mysqli_fetch_array($data)
						
					?>
					<?php $waktu = date('d M Y', strtotime($get['tanggal'])); ?>
					<div class="big-blog-item">
                        <h1 class="gs-title"><?= $get['judul']?></h1>
						<?="<img src='admin/image/".$get['foto']."'style='width:853px; height:480px;'>"?>
						<div class="blog-content text-box text-white">
							<h8 class="gs-meta"><?= $waktu ?>  /  Kategori <a><?= $get['kategori']?></a></h8><br/>
							<i class="gs-meta">by <?= $get['penulis']?></i>
							<p><?= $get['isi']?></p>
						</div>
					</div>
					

				</div>
				<div class="col-xl-3 col-lg-4 col-md-5 sidebar">
					<div id="stickySidebar">
							<div class="widget-item">
							<div class="categories-widget">
								<h4 class="widget-title">Kategori</h4>
								<ul>
									<li><a href="jenis_artikel.php?kategori=info%20kegiatan">Info Kegiatan</a></li>
									<li><a href="jenis_artikel.php?kategori=info%20beasiswa"">Info Beasiswa</a></li>
									<li><a href="jenis_artikel.php?kategori=prestasi">Prestasi</a></li>
									<li><a href="jenis_artikel.php?kategori=info%20kemahasiswaan">Informasi Kemahasiswaan</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('component/footer.php'); ?>