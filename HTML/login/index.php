<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>LogIn Form</title>
  <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Hind:300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
  
  <link href="../img/iconif.png" rel="icon" type="image/png"/>
  
  <link rel="stylesheet" href="./style.css">

  
</head>

<body>
    <!--cek pesan notifikasi-->
    <?php 
	if(isset($_GET['pesan'])){
		if($_GET['pesan'] == "gagal"){
			echo '<script language="javascript">';
            echo 'alert("Password Yang Anda Masukkan Salah!")';
            echo '</script>';
		}else if($_GET['pesan'] == "logout"){
			echo '<script language="javascript">';
            echo 'alert("Anda Telah Berhasil Logout")';
            echo '</script>';
		}else if($_GET['pesan'] == "belum_login"){
			echo '<script language="javascript">';
            echo 'alert("Anda Harus Login Terlebih Dahulu!")';
            echo '</script>';
		}
	}
	?>
    <!------------------------->

  <div id="login-button">
  <img src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png">
  </img>
</div>
<div id="container">
  <h1>Log In</h1>
  <span class="close-btn">
    <img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png"></img>
  </span>

  <form method="post" action="login.php">
    <table>
        <input type="username" name="username" placeholder="Masukkan Username" maxlength ="30" require >
        <input type="password" name="password" placeholder="Masukkan Password" maxlength="12" require>
        <input type="submit" value = "login" >
    </table>
    </form>
</div>

<script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script  src="./script.js"></script>
</body>

</html>
