<?php include('component/header.php'); ?>


	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg/7.jpg">
		<div class="page-info">
			<h2>Daftar Prestasi</h2>
			<div class="site-breadcrumb">
				<a href="">Home</a>  /
				<span>Prestasi</span>
			</div>
		</div>
	</section>
	<!-- Page top end-->


	<!-- Review section -->
	<section class="review-section">
		<div class="container">

			<table class="table table-hover table-dark">
			<thead>
				<tr>
				<th scope="col">Tahun</th>
				<th scope="col">Nama</th>
				<th scope="col">Prestasi</th>
				<th scope="col">Event</th>
				</tr>
			</thead>
			<tbody>
				<tr>
				<?php
					include("admin/query/connectDB.php");
					$query2     = mysqli_query($link, "select * from prestasi");
					$jmldata    = mysqli_num_rows($query2);
					$paginasi = true;
					// Langkah 1. Tentukan batas,cek halaman & posisi data
					$batas   = 5;
					if (isset($_GET['halaman'])) {
						$halaman = $_GET['halaman'];
					}
					if(empty($halaman)){
					 $posisi  = 0;
					$halaman = 1;
					}
					else{
						$posisi  = ($halaman-1) * $batas;
					}

					$query = "SELECT * FROM prestasi";
					$hasil = mysqli_query($link, $query);
					if (mysqli_num_rows($hasil) > 0) {
						$query = "SELECT * FROM prestasi LIMIT $posisi,$batas";
						$hasil = mysqli_query($link, $query);
						$i = 0;
						while ($data = mysqli_fetch_assoc($hasil)) {
					?>
				<tr>
					<td><?php echo $data['tahun'] ?></td>
					<td><?php echo $data['nama'] ?></td>
					<td><?php echo $data['prestasi'] ?></td>
                    <td><?php echo $data['kegiatan'] ?></td>
				</tr>
				<?php $i = $i + 1; }
				?>
			<?php } else { ?>

			<?php
				echo "No data available.";
			}
			?>
			</tbody>
			
			</table>
			 

		</div>
	</section>
	<!-- Review section end-->


	<?php include('component/footer.php'); ?>