<?php include('component/header.php'); ?>

	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg/8.jpg">
		<div class="page-info">
			<h2>Contact</h2>
			<div class="site-breadcrumb">
				<a href="">Home</a>  /
				<span>Contact</span>
			</div>
		</div>
	</section>
	<!-- Page top end-->

	<!-- Contact page -->
	<section class="contact-page">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 order-2 order-lg-1">
					<form class="contact-form" method="POST" action="kirimPesan.php">
						<input type="text" placeholder="Nama Lengkap" maxlength="50" name="saran_nama" required>
						<input type="email" placeholder="Alamat Email" maxlength="50" name="saran_email" required>
						<input type="text" placeholder="Perihal" maxlength="200" name="saran_perihal" required>
						<textarea name="saran_isi" placeholder="Isi Pesan" required></textarea>
						<button class="site-btn">Kirim Pesan<img src="img/icons/double-arrow.png" alt="#"/></button>
					</form>
				</div>
				<div class="col-lg-5 order-1 order-lg-2 contact-text text-white">
					<h3>Kontak Kami</h3>
					<p>Bila ingin mengetahui informasi lebih lanjut, Silahkan hubungi atau datang ke Sekretariat kami.</p>
					<div class="cont-info">
						<div class="ci-icon"><img src="img/icons/location.png" alt=""></div>
						<div class="ci-text">Sekretariat: Gedung Classroom Lt.1 Fakultas Teknik Universitas Hasanuddin, Gowa</div>
					</div>
					<div class="cont-info">
						<div class="ci-icon"><img src="img/icons/mail.png" alt=""></div>
						<div class="ci-text">okifftuh@gmail.com</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Contact page end-->


	<?php include('component/footer.php'); ?>