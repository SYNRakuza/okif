<!doctype html>
<html lang="en">

        <head>
                <title>Administrator website OKIF FT-UH</title>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
                <!-- VENDOR CSS -->
                <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
                <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
                <link rel="stylesheet" href="assets/vendor/linearicons/style.css">
                <link rel="stylesheet" href="assets/vendor/summernote/summernote.css">
                <link rel="stylesheet" href="assets/vendor/bootstrap-markdown/bootstrap-markdown.min.css">
				<link rel="stylesheet" href="assets/vendor/datatables/css-main/jquery.dataTables.min.css">
				<link rel="stylesheet" href="assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
				<link rel="stylesheet" href="assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
                <!-- MAIN CSS -->
                <link rel="stylesheet" href="assets/css/main.css">
                <!-- GOOGLE FONTS -->
                <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
                <!-- ICONS -->
                <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
                <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">



                <!-- include summernote css/js -->
                <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
                <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
            </head>

<body>
<?php 
	session_start();
	if($_SESSION['status']!="login"){
		header("location:../login/index.php?pesan=belum_login");
	}
	
	?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.php"><img src="assets/img/if.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div id="tour-fullwidth" class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<form class="navbar-form navbar-left search-form">
					<input type="text" value="" class="form-control" placeholder="Search dashboard...">
					<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
				</form>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<?php include '../koneksi.php'  ?>

							<?php 
							$data = mysqli_query($koneksi,"SELECT * FROM admin");
							$get = mysqli_fetch_array($data)
							?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?="<img src='image/".$get['foto']."''>"?> <span>
								<?php 
								echo $_SESSION['username'];
								?>
							</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="../login/logout.php"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
