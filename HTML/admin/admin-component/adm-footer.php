</div>
	<!-- END WRAPPER -->
<!-- Javascript -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/summernote/summernote.min.js"></script>
<script src="assets/vendor/markdown/markdown.js"></script>
<script src="assets/vendor/to-markdown/to-markdown.js"></script>
<script src="assets/vendor/bootstrap-markdown/bootstrap-markdown.js"></script>
<script src="assets/scripts/klorofilpro-common.js"></script>

<!-- Javascript daftar prestasi -->
	<script src="assets/vendor/datatables/js-main/jquery.dataTables.min.js"></script>
	<script src="assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js"></script>
	<script src="assets/vendor/datatables-colreorder/dataTables.colReorder.js"></script>
	<script src="assets/vendor/datatables-tabletools/js/dataTables.tableTools.js"></script>


<script>
$(document).ready(function() {
$("#summernote").summernote({
  placeholder: 'enter directions here...',
        height: 300,
         callbacks: {
        onImageUpload : function(files, editor, welEditable) {

             for(var i = files.length - 1; i >= 0; i--) {
                     sendFile(files[i], this);
            }
        }
    }
    });
});
function sendFile(file, el) {
var form_data = new FormData();
form_data.append('file', file);
$.ajax({
    data: form_data,
    type: "POST",
    url: 'query/uploadImgToFolder.php',
    cache: false,
    contentType: false,
    processData: false,
    success: function(url) {
        $(el).summernote('editor.insertImage', url);
    }
});
}

// $(document).ready(function() {
// 	$('#summernote').summernote({
// 	    height: 300,
// 	    focus: true,
// 	    onpaste: function() {
// 	        alert('You have pasted something to the editor');
// 	    },
// 			callbacks : {
// 				onImageUpload: function(files, editor, welEditable) {
// 					sendFile(files[i], editor, welEditable);
// 					for(var i = files.length - 1; i >= 0; i--) {
// 									sendFileToFolder(files[i], this);
// 				 }
// 		    }
// 			}
// 	});
// 	function sendFileToFolder(file, el) {
// 	var form_data = new FormData();
// 	form_data.append('file', file);
// 	$.ajax({
// 	    data: form_data,
// 	    type: "POST",
// 	    url: 'query/tambahArtikel.php',
// 	    cache: false,
// 	    contentType: false,
// 	    processData: false,
// 	    success: function(url) {
// 	        $(el).summernote('editor.insertImage', url);
// 	    }
// 	});
// 	}
// 	function sendFile(file, editor, welEditable) {
//       data = new FormData();
//       data.append("file", file);
//       $.ajax({
//           data: data,
//           type: "POST",
//           url: "query/tambahArtikelphp",
//           cache: false,
//           contentType: false,
//           processData: false,
//           success: function(url) {
//               editor.insertImage(welEditable, url);
//       		}
//   		});
//   }
// });

// markdown editor
var initContent = '<h4>Hello there</h4> ' +
    '<p>How are you? I have below task for you :</p> ' +
    '<p>Select from this text... Click the bold on THIS WORD and make THESE ONE italic, ' +
    'link GOOGLE to google.com, ' +
    'test to insert image (and try to tab after write the image description)</p>' +
    '<p>Test Preview And ending here...</p> ' +
    '<p>Click "List"</p> Enjoy!';

$('#markdown-editor').text(toMarkdown(initContent));


//javascript daftar prestasi
	$(function() {
		// datatable with paging options and live search
		$('#featured-datatable').dataTable({
			sDom: "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
		});
	});


</script>


</body>

</html>
