<?php include('admin-component/adm-header.php') ?>

		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-pencil"></i> <span>Artikel</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse ">
								<ul class="nav">
									<li><a href="index.php">Tulis</a></li>
									<li><a href="semua-artikel.php">Semua Artikel</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Tentang OKIF FT-UH</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="sejarah.php">Sejarah</a></li>
									<li><a href="ketentuanumum.php" >Ketentuan Umum</a></li>
									<li><a href="tujuanusaha.php">Tujuan dan Usaha</a></li>
									<li><a href="fungsiwewenang.php">Fungsi dan Wewenang</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-user"></i> <span>Pengurus</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="pengurus-dmmif.php">DMMIF FT-UH</a></li>
									<li><a href="pengurus-hmif.php">HMIF FT-UH</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#prestasis" data-toggle="collapse" class="active"><i class="lnr lnr-list"></i> <span>Prestasi</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="prestasis" class="collapse in">
								<ul class="nav">
									<li><a href="input-prestasi.php">Input Prestasi</a></li>
									<li><a href="daftar-prestasi.php" class="active">Daftar Prestasi</a></li>
								</ul>
							</div>
						</li>
						<li >
							<a href="tambah_admin.php" class="collapsed"><i class="lnr lnr-user"><span>Admin</span></i></a>
						</li>
						<li>
							<a href="saran_masuk.php" class="collapsed"><i class="lnr lnr-envelope"> <span>Saran Masuk</span></i></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
					<!-- FEATURED DATATABLE -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Daftar Prestasi</h3>
						</div>
						<div class="panel-body">
							<table  class="table table-striped table-hover">
								<!-- id="featured-datatable" -->
								<thead>
									<tr>
										<th>Tahun</th>
										<th>Nama</th>
										<th>Prestasi</th>
                                        <th>Event</th>
									</tr>
								</thead>
								<tbody>
									<?php
									include("query/connectDB.php");
									$query2     = mysqli_query($link, "select * from prestasi");
									$jmldata    = mysqli_num_rows($query2);
									$paginasi = true;
									// Langkah 1. Tentukan batas,cek halaman & posisi data
									$batas   = 5;
									if (isset($_GET['halaman'])) {
										$halaman = $_GET['halaman'];
									}
									if(empty($halaman)){
									 $posisi  = 0;
									 $halaman = 1;
									}
									else{
										$posisi  = ($halaman-1) * $batas;
									}

									$query = "SELECT * FROM prestasi";
									$hasil = mysqli_query($link, $query);
									if (mysqli_num_rows($hasil) > 0) {
											$query = "SELECT * FROM prestasi LIMIT $posisi,$batas";
											$hasil = mysqli_query($link, $query);
											$i = 0;
											while ($data = mysqli_fetch_assoc($hasil)) {
									 ?>
									<tr>
										<td><?php echo $data['tahun'] ?></td>
										<td><?php echo $data['nama'] ?></td>
										<td><?php echo $data['prestasi'] ?></td>
                    <td><?php echo $data['kegiatan'] ?></td>
									</tr>
								<?php $i = $i + 1; }
								?>
								</tbody>
							</table>
							Showing 1 to <?php echo $i ?> of <?php echo $jmldata ?> entries
							<?php } else { ?>
							 </tbody>
						 </table>
						 <?php
						 		echo "No data available.";
							}
						 ?>
						</div>
						<div class="text-center">
							<ul class="pagination">
								<!-- Langkah 3: Hitung total data dan halaman serta link 1,2,3  -->
								<?php
								if ($paginasi) {
									$query2     = mysqli_query($link, "select * from prestasi");
									$jmldata    = mysqli_num_rows($query2);
									$jmlhalaman = ceil($jmldata/$batas);

									$previous = $halaman - 1;
									if ($previous > 0){
										echo "<li><a href='daftar-prestasi.php?halaman=$previous'><i class='fa fa-angle-left'></i><span class='sr-only'>Previous</span></a></li>";
									}
									for($i=1;$i<=$jmlhalaman;$i++) {
										if ($i != $halaman){
											echo " <li><a href=\"daftar-prestasi.php?halaman=$i\">$i</a></li>";
										} else {
											echo " <li class='active'><a>$i</a><li>";
										}
									}
									$next = $halaman + 1;
									if ($next <= $jmlhalaman){
										echo "<li><a href='daftar-prestasi.php?halaman=$next'><i class='fa fa-angle-right'></i><span class='sr-only'>Next</span></a></li>";
									}
								}
								?>
							</ul>
						</div>
					</div>
					<!-- END FEATURED DATATABLE --
		</div>
		<!-- END MAIN -->

<?php include('admin-component/adm-footer.php') ?>
