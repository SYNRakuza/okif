<?php include('admin-component/adm-header.php') ?>
<?php include '../koneksi.php'  ?>
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a href="#dashboards" data-toggle="collapse"><i class="lnr lnr-pencil"></i> <span>Artikel</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="index.php">Tulis</a></li>
									<li><a href="semua-artikel.php" >Semua Artikel</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Tentang OKIF FT-UH</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="sejarah.php">Sejarah</a></li>
									<li><a href="ketentuanumum.php" >Ketentuan Umum</a></li>
									<li><a href="tujuanusaha.php">Tujuan dan Usaha</a></li>
									<li><a href="fungsiwewenang.php">Fungsi dan Wewenang</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-user"></i> <span>Pengurus</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="pengurus-dmmif.php">DMMIF FT-UH</a></li>
									<li><a href="pengurus-hmif.php">HMIF FT-UH</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#prestasis" data-toggle="collapse" class="collapsed"><i class="lnr lnr-list"></i> <span>Prestasi</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="prestasis" class="collapse ">
								<ul class="nav">
									<li><a href="input-prestasi.php">Input Prestasi</a></li>
									<li><a href="daftar-prestasi.php">Daftar Prestasi</a></li>
								</ul>
							</div>
						</li>
						<li >
							<a href="tambah_admin.php" class="collapsed"><i class="lnr lnr-user"><span>Admin</span></i></a>
						</li>
						<li>
							<a href="saran_masuk.php" class="active"><i class="lnr lnr-envelope"> <span>Saran Masuk</span></i></a>
						</li>
						
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
					<!-- FEATURED DATATABLE -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Saran Masuk</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-hover">
								<!-- id="featured-datatable" -->
				<thead>
	                  <tr>
	                      <th>Id</th>
	                      <th>Nama Pengirim</th>
	                      <th>Email Pengirim</th>
	                      <th>Perihal</th>
	                      <th>Isi Saran</th>
	                      <th>Aksi</th>
                  </tr>
				</thead>

				<tbody>
					<?php
						$data = mysqli_query($koneksi,"SELECT * FROM saran");
						$no = 1;
						while($get = mysqli_fetch_array($data)){
					?>
					<tr>
						<td><?= $no++ ?></td>
						<td><?= $get['saran_nama'] ?></td>
						<td><?= $get['saran_email'] ?></td>
						<td><?= $get['saran_perihal'] ?></td>
						<td><?= $get['saran_isi'] ?></td>
						<td>
							<a href="hapus_saran.php?id=<?php echo $get['id_saran']; ?>" >
								<i class="lnr lnr-trash"></i>
							</a>
						</td>
					</tr>

				<?php } ?>

				</tbody>
							</table>


						</div>
						</div>
					</div>
					<!-- END FEATURED DATATABLE -->
		</div>
		<!-- END MAIN -->

<?php include('admin-component/adm-footer.php') ?>
