<?php include('admin-component/adm-header.php') ?>

		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-pencil"></i> <span>Artikel</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="index.php">Tulis</a></li>
									<li><a href="semua-artikel.php">Semua Artikel</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#dashboards" data-toggle="collapse" class="active"><i class="lnr lnr-file-empty"></i> <span>Tentang OKIF FT-UH</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse in">
								<ul class="nav">
									<li><a href="sejarah.php">Sejarah</a></li>
									<li><a href="ketentuanumum.php" class="active">Ketentuan Umum</a></li>
									<li><a href="tujuanusaha.php">Tujuan dan Usaha</a></li>
									<li><a href="fungsiwewenang.php">Fungsi dan Wewenang</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-user"></i> <span>Pengurus</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="pengurus-dmmif.php">DMMIF FT-UH</a></li>
									<li><a href="pengurus-hmif.php">HMIF FT-UH</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#prestasis" data-toggle="collapse" class="collapsed"><i class="lnr lnr-list"></i> <span>Prestasi</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="prestasis" class="collapse">
								<ul class="nav">
									<li><a href="input-prestasi.php">Input Prestasi</a></li>
									<li><a href="daftar-prestasi.php">Daftar Prestasi</a></li>
								</ul>
							</div>
						</li>
						<li >
							<a href="tambah_admin.php" class="collapsed"><i class="lnr lnr-user"><span>Admin</span></i></a>
						</li>
						<li>
							<a href="saran_masuk.php" class="collapsed"><i class="lnr lnr-envelope"> <span>Saran Masuk</span></i></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                <!-- MAIN CONTENT -->
                <div class="main-content">
                    <div class="container-fluid">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="#">Tentang</a></li>
                            <li class="active">Ketentuan Umum</li>
                        </ul>
                        <h1 class="page-title">Ketentuan Umum</h1>
												<form action="query/tambahKetentuan.php" method="post">
													<textarea name="ketentuan" id="summernote"></textarea>
													<button name="submit" type="submit" class="btn btn-primary">SIMPAN</button>
                        </form>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->

<?php include('admin-component/adm-footer.php') ?>
