<?php
  if(isset($_POST['submit'])){
    include("connectDB.php");
    $id = $_GET['id'];

    $time = time();
    $nama = $_POST['nama'];
    $jabatan = $_POST['jabatan'];
    $periode = $_POST['periode'];

    $nama_foto = $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];
    $format_foto = $_FILES['foto']['type'];
    $size_foto = $_FILES['foto']['size'];

    if ($nama_foto != null){
      if ($format_foto == 'image/jpeg' || $format_foto == 'image/png'){
        if ($size_foto <= 10000000) {
          if (file_exists("../image/".$nama_foto)) {
            if ($format_foto == 'image/jpeg') {
              $nama_foto = str_replace(".jpg", "", $nama_foto);
              $nama_foto = $nama_foto . "_" . $time . ".jpg";
            } else if ($format_foto == 'image/png') {
              $nama_foto = str_replace(".png", "", $nama_foto);
              $nama_foto = $nama_foto . "_" . $time . ".png";
            }
          }
          move_uploaded_file($lokasi_foto, '../image/'. $nama_foto);
          $query = "SELECT * FROM hmif WHERE id='$id'";
          $hasil = mysqli_query($link, $query);
          $data = mysqli_fetch_assoc($hasil);
          $nama_gambar = $data['foto'];

          //hapus gambar di folder
          $target = "../image/". $nama_gambar;
          if (file_exists($target)) {
            unlink($target);
          }
          $query = "UPDATE hmif SET foto='$nama_foto' WHERE id='$id'";
          if (mysqli_query($link, $query)) {
            // echo "<script>alert('Berhasil ganti gambar!');</script>";
          } else {
            echo "<script>alert('Gagal ganti gambar!');</script>";
            echo "<script>location='../pengurus-hmif.php';</script>";
          }
        } else {
          echo "<script>alert('Ukuran gambar melebihi 10 Mb');</script>";
          echo "<script>location='../pengurus-hmif.php';</script>";
        }
      } else {
        echo "<script>alert('Format yang anda masukkan salah');</script>";
        echo "<script>location='../pengurus-hmif.php';</script>";
      }
    }
      $query = "UPDATE hmif SET nama='$nama', jabatan='$jabatan', periode='$periode'
              WHERE id='$id'";
      if (mysqli_query($link, $query)){
        echo "<script>alert('Data Berhasil disunting!');</script>";
        echo "<script>location='../pengurus-hmif.php';</script>";
      } else {
        echo "<script>alert('Data gagal disunting !');</script>";
        echo "<script>location='../pengurus-hmif.php';</script>";
      }
    }
 ?>
