<?php
  if (isset($_POST['submit'])) {
    include("connectDB.php");

    $time = time();
    $nama = $_POST['nama'];
    $jabatan = $_POST['jabatan'];
    $periode = $_POST['periode'];
    $nama_foto = $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];
    $format_foto = $_FILES['foto']['type'];
    $size_foto = $_FILES['foto']['size'];

    if ($format_foto == 'image/jpeg' || $format_foto == 'image/png') {
      if ($size_foto <= 10000000) {
        if (file_exists("../image/".$nama_foto)) {
          if ($format_foto == 'image/jpeg') {
            $nama_foto = str_replace(".jpg", "", $nama_foto);
            $nama_foto = $nama_foto . "_" . $time . ".jpg";
          } else if ($format_foto == 'image/png') {
            $nama_foto = str_replace(".png", "", $nama_foto);
            $nama_foto = $nama_foto . "_" . $time . ".png";
          }
        }
        move_uploaded_file($lokasi_foto, '../image/'. $nama_foto);
        $query = "INSERT INTO hmif (nama, jabatan, periode, foto)
                VALUES ('$nama', '$jabatan', '$periode', '$nama_foto')";

        if (mysqli_query($link, $query)){
          echo "<script>alert('Data berhasil ditambahkan');</script>";
          echo "<script>location='../pengurus-hmif.php';</script>";
        } else {
            echo "<script>alert('Data gagal ditambahkan');</script>";
            echo "<script>location='../pengurus-hmif.php';</script>";
        }
      } else {
        echo "<script>alert('Ukuran gambar melebihi 10 Mb');</script>";
        echo "<script>location='../pengurus-hmif.php';</script>";
      }
    } else {
      echo "<script>alert('Format gambar yang anda masukkan salah');</script>";
      echo "<script>location='../pengurus-hmif.php';</script>";
    }
  }
 ?>
