<?php
  if (isset($_POST['submit'])) {
    include("connectDB.php");

	  $time = time();
    $nama 	= $_POST['nama_lengkap_admin'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $nama_foto = $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];
    $format_foto = $_FILES['foto']['type'];
    $size_foto = $_FILES['foto']['size'];

    if ($format_foto == 'image/jpeg' || $format_foto == 'image/png') {
      if ($size_foto <= 1000000) {
        if (file_exists("../image/".$nama_foto)) {
          if ($format_foto == 'image/jpeg') {
            $nama_foto = str_replace(".jpg", "", $nama_foto);
            $nama_foto = $nama_foto . "_" . $time . ".jpg";
          } else if ($format_foto == 'image/png') {
            $nama_foto = str_replace(".png", "", $nama_foto);
            $nama_foto = $nama_foto . "_" . $time . ".png";
          }
        }
        move_uploaded_file($lokasi_foto, '../image/'. $nama_foto);
        $query = "INSERT INTO admin (id_admin, username, password, nama_lengkap_admin, foto)
              VALUES ('', '$username', '$password', '$nama', '$nama_foto')";

        if (mysqli_query($link, $query)){
          echo "<script>alert('Data berhasil ditambahkan');</script>";
          echo "<script>location='../tambah_admin.php';</script>";
        } 
        else {
            echo "<script>alert('Data gagal ditambahkan');</script>";
            echo "<script>location='../tambah_admin.php';</script>";
        }
      } else {
        echo "<script>alert('Ukuran gambar melebihi 1 Mb');</script>";
        echo "<script>location='../tambah_admin.php';</script>";
      }
    } else {
      echo "<script>alert('Format gambar yang anda masukkan salah');</script>";
      echo "<script>location='../tambah_admin.php';</script>";
    }
  }
 ?>
