<?php
  if (isset($_POST['submit'])) {
    include("connectDB.php");

    $time = time();
    $judul = $_POST['judul'];
    $kategori = $_POST['kategori'];
    $tanggal = $_POST['tanggal'];
    $nama_foto = $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];
    $format_foto = $_FILES['foto']['type'];
    $size_foto = $_FILES['foto']['size'];
    $isi = $_POST['isi'];
    $penulis = $_POST['penulis'];

    if ($format_foto == 'image/jpeg' || $format_foto == 'image/png') {
      if ($size_foto <= 4000000) {
        if (file_exists("../image/".$nama_foto)) {
          if ($format_foto == 'image/jpeg') {
            $nama_foto = str_replace(".jpg", "", $nama_foto);
            $nama_foto = $nama_foto . "_" . $time . ".jpg";
          } else if ($format_foto == 'image/png') {
            $nama_foto = str_replace(".png", "", $nama_foto);
            $nama_foto = $nama_foto . "_" . $time . ".png";
          }
        }
        move_uploaded_file($lokasi_foto, '../image/'. $nama_foto);
        $query = "INSERT INTO artikel (judul, isi, foto, kategori, tanggal, penulis)
        VALUES ('$judul', '$isi', '$nama_foto', '$kategori', '$tanggal', '$penulis')";

    if (mysqli_query($link, $query)) {
      echo "<script>alert('Berhasil ditambahkan');</script>";
      echo "<script>location='../semua-artikel.php';</script>";
    } else {
      echo "<script>alert('Gagal ditambahkan');</script>";
      echo "<script>location='../semua-artikel.php';</script>";
    }
  } else {
        echo "<script>alert('Ukuran gambar melebihi 4 Mb');</script>";
        echo "<script>location='../index.php';</script>";
      }
    } else {
      echo "<script>alert('Format gambar yang anda masukkan salah');</script>";
      echo "<script>location='../index.php';</script>";
    }
  }

 ?>
