<?php
  if(isset($_POST['submit'])){
    include("connectDB.php");
    $id = $_GET['id'];

    $time = time();
    $judul = $_POST['judul'];
    $kategori = $_POST['kategori'];
    $tanggal = $_POST['tanggal'];
    $nama_foto = $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];
    $format_foto = $_FILES['foto']['type'];
    $size_foto = $_FILES['foto']['size'];
    $isi = $_POST['isi'];
    $penulis = $_POST['penulis'];


    if ($nama_foto != null){
      if ($format_foto == 'image/jpeg' || $format_foto == 'image/png'){
        if ($size_foto <= 4000000) {
          if (file_exists("../image/".$nama_foto)) {
            if ($format_foto == 'image/jpeg') {
              $nama_foto = str_replace(".jpg", "", $nama_foto);
              $nama_foto = $nama_foto . "_" . $time . ".jpg";
            } else if ($format_foto == 'image/png') {
              $nama_foto = str_replace(".png", "", $nama_foto);
              $nama_foto = $nama_foto . "_" . $time . ".png";
            }
          }
          move_uploaded_file($lokasi_foto, '../image/'. $nama_foto);
          $query = "SELECT * FROM artikel WHERE id='$id'";
          $hasil = mysqli_query($link, $query);
          $data = mysqli_fetch_assoc($hasil);
          $nama_gambar = $data['foto'];

          //hapus gambar di folder
          $target = "../image/". $nama_gambar;
          if (file_exists($target)) {
            unlink($target);
          }
          $query = "UPDATE artikel SET foto='$nama_foto' WHERE id='$id'";
          if (mysqli_query($link, $query)) {
            // echo "<script>alert('Berhasil ganti gambar!');</script>";
          } else {
            echo "<script>alert('Gagal ganti gambar!');</script>";
            echo "<script>location='../semua-artikel.php';</script>";
          }
        } else {
          echo "<script>alert('Ukuran gambar melebihi 4 Mb');</script>";
          echo "<script>location='../semua-artikel.php';</script>";
        }
      } else {
        echo "<script>alert('Format yang anda masukkan salah');</script>";
        echo "<script>location='../semua-artikel.php';</script>";
      }
    }
      $query = "UPDATE artikel SET judul='$judul', foto='$nama_foto', kategori='$kategori', tanggal='$tanggal', isi='$isi', penulis='$penulis'
              WHERE id='$id'";
      if (mysqli_query($link, $query)){
        echo "<script>alert('Data Berhasil disunting!');</script>";
        echo "<script>location='../semua-artikel.php';</script>";
      } else {
        echo "<script>alert('Data gagal disunting !');</script>";
        echo "<script>location='../semua-artikel.php';</script>";
      }
    }
 ?>