<?php include('admin-component/adm-header.php') ?>



		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a href="#dashboards" data-toggle="collapse" class="active"><i class="lnr lnr-pencil"></i> <span>Artikel</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse in">
								<ul class="nav">
									<li><a href="index.php" class="active">Tulis</a></li>
									<li><a href="semua-artikel.php">Semua Artikel</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Tentang OKIF FT-UH</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="sejarah.php">Sejarah</a></li>
									<li><a href="ketentuanumum.php" >Ketentuan Umum</a></li>
									<li><a href="tujuanusaha.php">Tujuan dan Usaha</a></li>
									<li><a href="fungsiwewenang.php">Fungsi dan Wewenang</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-user"></i> <span>Pengurus</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="pengurus-dmmif.php">DMMIF FT-UH</a></li>
									<li><a href="pengurus-hmif.php">HMIF FT-UH</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#prestasis" data-toggle="collapse" class="collapsed"><i class="lnr lnr-list"></i> <span>Prestasi</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="prestasis" class="collapse">
								<ul class="nav">
									<li><a href="input-prestasi.php">Input Prestasi</a></li>
									<li><a href="daftar-prestasi.php">Daftar Prestasi</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="tambah_admin.php"><i class="lnr lnr-user"> <span>Admin</span></i></a>
						</li>
						<li>
							<a href="saran_masuk.php"><i class="lnr lnr-envelope"> <span>Saran Masuk</span></i></a>
						</li>
						
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                <!-- MAIN CONTENT -->
                <div class="main-content">
                    <div class="container-fluid">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="#">Artikel</a></li>
                            <li class="active">Tulis</li>
                        </ul>
                        <h1 class="page-title">Artikel</h1>
												<form action="query/tambahArtikel.php"  method="post" enctype="multipart/form-data">
													<input name="judul" class="form-control input-lg" placeholder="Judul" type="text" required><br>
													<!-- <div class="summernote"> -->
														<!-- <p>Isi artikel...</p> -->
													<label>Input File</label>
													<input type="file" name="foto" required>
													<p class="help-block"><em>Valid file type: .jpg, .png. File size max: 4 MB</em></p>
													<label>Jenis Kategori</label>
													<select name="kategori" class="form-control" required>
														<option value="info kegiatan">Info Kegiatan</option>
														<option value="info beasiswa">Info Beasiswa</option>
														<option value="prestasi">Prestasi</option>
														<option value="info kemahasiswaan">Info Kemahasiswaan</option>
													</select>
													<label>Tanggal Postingan</label>
													<input type="date" name="tanggal">
													<textarea name="isi" id="summernote" required=""></textarea>
													<label>Penulis</label>
													<input type="text" name="penulis" required>
													<!-- </div> -->
													<button name="submit" type="submit" class="btn btn-primary">SUBMIT</button>
												</form>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->

<?php include('admin-component/adm-footer.php') ?>
