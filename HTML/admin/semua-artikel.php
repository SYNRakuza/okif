<?php include('admin-component/adm-header.php') ?>

		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a href="#dashboards" data-toggle="collapse" class="active"><i class="lnr lnr-pencil"></i> <span>Artikel</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse in">
								<ul class="nav">
									<li><a href="index.php">Tulis</a></li>
									<li><a href="semua-artikel.php" class="active">Semua Artikel</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Tentang OKIF FT-UH</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="sejarah.php">Sejarah</a></li>
									<li><a href="ketentuanumum.php" >Ketentuan Umum</a></li>
									<li><a href="tujuanusaha.php">Tujuan dan Usaha</a></li>
									<li><a href="fungsiwewenang.php">Fungsi dan Wewenang</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-user"></i> <span>Pengurus</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="pengurus-dmmif.php">DMMIF FT-UH</a></li>
									<li><a href="pengurus-hmif.php">HMIF FT-UH</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#prestasis" data-toggle="collapse" class="collapsed"><i class="lnr lnr-list"></i> <span>Prestasi</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="prestasis" class="collapse ">
								<ul class="nav">
									<li><a href="input-prestasi.php">Input Prestasi</a></li>
									<li><a href="daftar-prestasi.php">Daftar Prestasi</a></li>
								</ul>
							</div>
						</li>
						<li >
							<a href="tambah_admin.php" class="collapsed"><i class="lnr lnr-user"> <span>Admin</span></i></a>
						</li>
						<li>
							<a href="saran_masuk.php" class="collapsed"><i class="lnr lnr-envelope"> <span>Saran Masuk</span></i></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
					<!-- FEATURED DATATABLE -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Semua Artikel</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-hover">
								<!-- id="featured-datatable" -->
								<thead>
                  <tr>
                      <th>id</th>
                      <th>Judul</th>
                      <th>Gambar</th>
                      <th>Jenis Kategori</th>
                      <th>Tanggal Postingan</th>
                      <th>Isi</th>
                      <th>Penulis</th>
                      <th>Aksi</th>
                  </tr>
								</thead>
								<tbody>
									<?php

									include("query/connectDB.php");
									$query2     = mysqli_query($link, "select * from artikel");
									$jmldata    = mysqli_num_rows($query2);
									$paginasi = true;
									// Langkah 1. Tentukan batas,cek halaman & posisi data
									$batas   = 5;
									if (isset($_GET['halaman'])) {
										$halaman = $_GET['halaman'];
									}
									if(empty($halaman)){
									 $posisi  = 0;
									 $halaman = 1;
									}
									else{
										$posisi  = ($halaman-1) * $batas;
									}

										$query = "SELECT * FROM artikel";
										$hasil = mysqli_query($link, $query);
										if (mysqli_num_rows($hasil) > 0) {
											$i = 1;
											$query = "SELECT * FROM artikel LIMIT $posisi, $batas";
											$hasil = mysqli_query($link, $query);
											while ($data = mysqli_fetch_assoc($hasil)) {
									 ?>
                    <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $data['judul'] ?></td>
                        <td><img src="image/<?php echo $data['foto'] ?>" width="20%"></td>
                        <td><?php echo $data['kategori'] ?></td>
                        <td><?php echo $data['tanggal'] ?></td>
                        <td><?php echo $data['isi'] ?></td>
                        <td><?php echo $data['penulis'] ?></td>
                        <td>
													<a type="button" data-toggle="modal" data-target="#sunting<?php echo $data['id']; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>Sunting</a><br>
													<br><a class="btn btn-danger" data-toggle="modal" data-target="#hapus<?php echo $data['id']; ?>"><i class="fa fa-trash"></i>Hapus&nbsp;&nbsp;&nbsp;</a>
													</td>
											</tr>
											<!-- POPUP SUNTING -->
											<div class="modal fade" id="sunting<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<button style="float:right; background: red; color : white;" type="button" class="close-button" data-dismiss="modal">
															<i class="fa fa-times"></i>
														</button>
														<div class="modal-header">
															<h4 class="modal-title" id="exampleModalLabel">
																<i class="fa fa-pencil"></i> Sunting Artikel
															</h4>
														</div>
														<div class="modal-body">
															<form action="query/updateArtikel.php?id=<?php echo $data['id']; ?>" method="post" enctype="multipart/form-data">
																<div class="form-group">
																	<label  class="form-control-label">
																		Judul
																	</label>
																	<input name="judul" value="<?php echo $data['judul']; ?>" type="text" class="form-control" id="">
																</div>
																<div class="form-group">
																	<label for="exampleInputFile">Foto</label> <br>
																		<img src="image/<?php echo $data['foto']; ?>"align='center' width='200px' height='200px'>
																		<br> Ganti gambar?
																		<input name="foto" type="file" class="customFile" class="custom-file-input">
																		<p class="help-block"><em>Valid file type: .jpg, .png. File size max: 4 MB</em></p>
																</div>
																<div class="form-group">
																	<label>
																		Jenis Kategori
																	</label>
																	<select name="kategori" class="form-control">
																		<option value="info kegiatan">Info Kegiatan</option>
																		<option value="info beasiswa">Info Beasiswa</option>
																		<option value="prestasi">Prestasi</option>
																		<option value="info kemahasiswaan">Info Kemahasiswaan</option>
																	</select>
																</div>
																<div class="form-group">
																	<label>
																		Tanggal Postingan
																	</label>
																	<input type="date" name="tanggal" class="form-control" value="<?php echo $data['tanggal']; ?>" > 
																</div>
																<div class="form-group">
																	<label  class="form-control-label">
																		Isi
																	</label>
																	<textarea id="summernote" name="isi" class="form-control" ><?php echo $data['isi']; ?></textarea>
																</div>
																<div class="form-group">
																	<label>
																		Penulis
																	</label>
																	<input type="text" name="penulis" value="<?php echo $data['penulis']; ?>">
																</div>
																<div class="modal-footer">
																	 <!-- data-dismiss="modal" data-toggle="modal" data-target="#berhasil-sunting" -->
																	<button name="submit" type="submit" class="btn btn-primary">
																		Simpan
																	</button>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
											<!-- END POPUP SUNTING -->
											<!-- POPUP BERHASIL SUNTING -->
											<div class="modal fade" id="berhasil-sunting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document" id="modal-info">
													<div class="modal-content">
														<button type="button" class="close-button" data-dismiss="modal">
															<i class="fa fa-times"></i>
														</button>
														<div class="modal-header">
															<h4 class="modal-title" id="exampleModalLabel">
																<i class="fa fa-pencil"></i> Sunting Artikel
															</h4>
														</div>
														<div class="modal-body" id="popup-info">
															<div class="alert alert-info alert-dismissible" role="alert">
																Artikel berhasil disunting
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- END POPUP BERHASIL SUNTING -->
											<!-- POPUP HAPUS -->
											<div class="modal fade" id="hapus<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document" id="modal-small">
													<div class="modal-content">
														<button style="float:right; background: red; color : white;" type="button" class="close-button" data-dismiss="modal">
															<i class="fa fa-times"></i>
														</button>
														<div class="modal-header">
															<h4 class="modal-title" id="exampleModalLabel">
																<i class="fa fa-trash"></i> Hapus Artikel
															</h4>
														</div>
														<div class="modal-body" id="popup-hapus">
															<p class="text-center">Yakin ingin menghapus artikel ini?</p>
															<div class="text-center">
																<!-- data-toggle="modal" data-target="#berhasil-hapus" data-dismiss="modal" -->
																<a href="query/hapusArtikel.php?id=<?php echo $data['id']; ?>" type="button" class="btn btn-primary" style="margin-right: 20px">Ya</a>
																<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- END POPUP HAPUS -->
                    </tr>
									<?php  $i = $i + 1; }
									 ?>
								</tbody>
							</table>
							Showing 1 to <?php echo $i-1?> of <?php echo $jmldata ?> entries
							<?php } else { ?>
								</tbody>
							</table>
							<?php
									echo "No data available.";
								}
							 ?>
							<div class="text-center">
								<ul class="pagination">
									<!-- Langkah 3: Hitung total data dan halaman serta link 1,2,3  -->
									<?php
									if ($paginasi) {
										$query2     = mysqli_query($link, "select * from artikel");
										$jmldata    = mysqli_num_rows($query2);
										$jmlhalaman = ceil($jmldata/$batas);

										$previous = $halaman - 1;
										if ($previous > 0){
											echo "<li><a href='semua-artikel.php?halaman=$previous'><i class='fa fa-angle-left'></i><span class='sr-only'>Previous</span></a></li>";
										}
										for($i=1;$i<=$jmlhalaman;$i++) {
											if ($i != $halaman){
												echo " <li><a href=\"semua-artikel.php?halaman=$i\">$i</a></li>";
											} else {
												echo " <li class='active'><a>$i</a><li>";
											}
										}
										$next = $halaman + 1;
										if ($next <= $jmlhalaman){
											echo "<li><a href='semua-artikel.php?halaman=$next'><i class='fa fa-angle-right'></i><span class='sr-only'>Next</span></a></li>";
										}
									}
									?>
								</ul>
							</div>
						</div>
						</div>
					</div>
					<!-- END FEATURED DATATABLE -->
		</div>
		<!-- END MAIN -->

<?php include('admin-component/adm-footer.php') ?>
