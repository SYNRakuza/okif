<?php include('admin-component/adm-header.php') ?>

		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-pencil"></i> <span>Artikel</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="index.php">Tulis</a></li>
									<li><a href="semua-artikel.php">Semua Artikel</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#dashboards" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Tentang OKIF FT-UH</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="dashboards" class="collapse">
								<ul class="nav">
									<li><a href="sejarah.php">Sejarah</a></li>
									<li><a href="ketentuanumum.php" >Ketentuan Umum</a></li>
									<li><a href="tujuanusaha.php">Tujuan dan Usaha</a></li>
									<li><a href="fungsiwewenang.php">Fungsi dan Wewenang</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="active"><i class="lnr lnr-user"></i> <span>Pengurus</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse in">
								<ul class="nav">
									<li><a href="pengurus-dmmif.php">DMMIF FT-UH</a></li>
									<li><a href="pengurus-hmif.php" class="active">HMIF FT-UH</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#prestasis" data-toggle="collapse" class="collapsed"><i class="lnr lnr-list"></i> <span>Prestasi</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="prestasis" class="collapse ">
								<ul class="nav">
									<li><a href="input-prestasi.php">Input Prestasi</a></li>
									<li><a href="daftar-prestasi.php">Daftar Prestasi</a></li>
								</ul>
							</div>
						</li>
						<li >
							<a href="tambah_admin.php" class="collapsed"><i class="lnr lnr-user"><span>Admin</span></i></a>
						</li>
						<li>
							<a href="saran_masuk.php" class="collapsed"><i class="lnr lnr-envelope"> <span>Saran Masuk</span></i></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
							<!-- SUBMIT TICKETS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Input Pengurus HMIF FT-UH</h3>
								</div>
								<div class="panel-body">
									<form action="query/tambahDataHMIF.php" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
										<fieldset>
											<div class="form-group">
												<label for="ticket-name" class="col-sm-3 control-label">Nama</label>
												<div class="col-sm-9">
													<input name="nama" type="text" class="form-control" id="ticket-name" placeholder="Nama" required>
												</div>
											</div>
											<div class="form-group">
												<label for="ticket-priority" class="col-sm-3 control-label">Jabatan</label>
												<div class="col-sm-9">
													<select id="ticket-priority" name="jabatan" class="form-control">
														<option value="Ketua Umum">Ketua Umum</option>
														<option value="Wakil Ketua Umum">Wakil Ketua Umum</option>
														<option value="Sekretaris Umum">Sekretaris Umum</option>
														<option value="Bendahara Umum">Bendahara Umum</option>
														<option value="Koordinator Kompartemen Pendidikan">Koordinator Kompartemen Pendidikan</option>
														<option value="Kepala Biro Pengawasan Akademik">Kepala Biro Pengawasan Akademik</option>
														<option value="Kepala Biro Riset dan Teknologi">Kepala Biro Riset dan Teknologi</option>
														<option value="Koordinator Kompartemen Pengembangan Sumber Daya Mahasiswa">Koordinator Kompartemen Pengembangan Sumber Daya Mahasiswa</option>
														<option value="Kepala Biro Pengembangan Kader">Kepala Biro Pengembangan Kader</option>
														<option value="Kepala Biro Minat dan Bakat">Kepala Biro Minat dan Bakat</option>
														<option value="Koordinator Kompartemen Pengabdian Masyarakat">Koordinator Kompartemen Pengabdian Masyarakat</option>
														<option value="Koordinator Kompartemen Informasi dan Komunikasi">Koordinator Kompartemen Informasi dan Komunikasi</option>
														<option value="Kepala Biro Hubungan Masyarakat">Kepala Biro Hubungan Masyarakat</option>
														<option value="Kepala Biro Publikasi dan Komunikasi">Kepala Biro Publikasi dan Komunikasi</option>
														<option value="Koordinator Kompartemen Kesekretariatan">Koordinator Kompartemen Kesekretariatan</option>
														<option value="Kepala Biro Administrasi">Kepala Biro Administrasi</option>
														<option value="Kepala Biro Rumah Tangga">Kepala Biro Rumah Tangga</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="ticket-periode" class="col-sm-3 control-label">Periode</label>
												<div class="col-sm-9">
													<input name="periode" type="text" class="form-control" id="ticket-periode" placeholder="Periode" required>
												</div>
											</div>
											<div class="form-group">
												<label for="ticket-attachment" class="col-sm-3 control-label">Upload Foto</label>
												<div class="col-md-9">
													<input name="foto" type="file" id="ticket-attachment">
													<p class="help-block"><em>Valid file type: .jpg, .png. File size max: 10 MB</em></p>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<button name="submit" type="submit" class="btn btn-primary btn-block">Submit</button>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
                            <!-- END SUBMIT TICKETS -->


							<!-- BASIC TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Daftar Pengurus</h3>
								</div>
								<div class="panel-body">
									<table class="table">
										<thead>
											<tr>
												<th>Nama</th>
												<th>Jabatan</th>
												<th>Periode</th>
                                                <th>Foto</th>
                                                <th></th>
											</tr>
										</thead>
										<tbody>
											<?php
												include("query/connectDB.php");
												$query = "SELECT * FROM hmif";
												$hasil = mysqli_query($link, $query);
												if (mysqli_num_rows($hasil) > 0) {
													while ($data = mysqli_fetch_assoc($hasil)) {
											 ?>
											<tr>
												<td><?php echo $data['nama'] ?></td>
												<td><?php echo $data['jabatan'] ?></td>
												<td><?php echo $data['periode'] ?></td>
												<td><img src="image/<?php echo $data['foto'] ?>" align="center" width="20%"></td>
                        <td>
												<!-- <button type="button" class="btn btn-primary">edit</button>  <button type="button" class="btn btn-danger">hapus</button></td> -->
												<a type="button" data-toggle="modal" data-target="#sunting<?php echo $data['id']; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>Sunting</a><br>
												<br><a class="btn btn-danger" data-toggle="modal" data-target="#hapus<?php echo $data['id']; ?>"><i class="fa fa-trash"></i>Hapus&nbsp;&nbsp;&nbsp;</a>
												</td>
										</tr>
										<!-- POPUP SUNTING -->
										<div class="modal fade" id="sunting<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<button style="float:right; background: red; color : white;" type="button" class="close-button" data-dismiss="modal">
														<i class="fa fa-times"></i>
													</button>
													<div class="modal-header">
														<h4 class="modal-title" id="exampleModalLabel">
															<i class="fa fa-pencil"></i> Sunting Data HMIF
														</h4>
													</div>
													<div class="modal-body">
														<form action="query/updateDataHMIF.php?id=<?php echo $data['id']; ?>" method="post" enctype="multipart/form-data">
															<div class="form-group">
																<label  class="form-control-label">
																	Nama
																</label>
																<input name="nama" value="<?php echo $data['nama']; ?>" type="text" class="form-control" id="">
															</div>
															<div class="form-group">
																<label  class="form-control-label">
																	Jabatan
																</label>
																<<select id="ticket-priority" name="jabatan" class="form-control">
																	<option value="Ketua Umum">Ketua Umum</option>
																	<option value="Wakil Ketua Umum">Wakil Ketua Umum</option>
																	<option value="Sekretaris Umum">Sekretaris Umum</option>
																	<option value="Bendahara Umum">Bendahara Umum</option>
																	<option value="Koordinator Kompartemen Pendidikan">Koordinator Kompartemen Pendidikan</option>
																	<option value="Kepala Biro Pengawasan Akademik">Kepala Biro Pengawasan Akademik</option>
																	<option value="Kepala Biro Riset dan Teknologi">Kepala Biro Riset dan Teknologi</option>
																	<option value="Koordinator Kompartemen Pengembangan Sumber Daya Mahasiswa">Koordinator Kompartemen Pengembangan Sumber Daya Mahasiswa</option>
																	<option value="Kepala Biro Pengembangan Kader">Kepala Biro Pengembangan Kader</option>
																	<option value="Kepala Biro Minat dan Bakat">Kepala Biro Minat dan Bakat</option>
																	<option value="Koordinator Kompartemen Pengabdian Masyarakat">Koordinator Kompartemen Pengabdian Masyarakat</option>
																	<option value="Koordinator Kompartemen Informasi dan Komunikasi">Koordinator Kompartemen Informasi dan Komunikasi</option>
																	<option value="Kepala Biro Hubungan Masyarakat">Kepala Biro Hubungan Masyarakat</option>
																	<option value="Kepala Biro Publikasi dan Komunikasi">Kepala Biro Publikasi dan Komunikasi</option>
																	<option value="Koordinator Kompartemen Kesekretariatan">Koordinator Kompartemen Kesekretariatan</option>
																	<option value="Kepala Biro Administrasi">Kepala Biro Administrasi</option>
																	<option value="Kepala Biro Rumah Tangga">Kepala Biro Rumah Tangga</option>
																</select>
															</div>
															<div class="form-group">
																<label  class="form-control-label">
																	Periode
																</label>
																<input name="periode" value="<?php echo $data['periode']; ?>" type="text" class="form-control" id="">
															</div>
															<div class="custom-file">
																	<label for="exampleInputFile">Foto</label> <br>
																	<img src="image/<?php echo $data['foto']; ?>"align='center' width='200px' height='200px'>
																	<br> Ganti gambar?
																	<input name="foto" type="file" class="customFile" class="custom-file-input">
																	<p class="help-block"><em>Valid file type: .jpg, .png. File size max: 10 MB</em></p>
															</div>
															<div class="modal-footer">
																 <!-- data-dismiss="modal" data-toggle="modal" data-target="#berhasil-sunting" -->
																<button name="submit" type="submit" class="btn btn-primary">
																	Simpan
																</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
										<!-- END POPUP SUNTING -->
										<!-- POPUP BERHASIL SUNTING -->
										<div class="modal fade" id="berhasil-sunting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document" id="modal-info">
												<div class="modal-content">
													<button type="button" class="close-button" data-dismiss="modal">
														<i class="fa fa-times"></i>
													</button>
													<div class="modal-header">
														<h4 class="modal-title" id="exampleModalLabel">
															<i class="fa fa-pencil"></i> Sunting Data
														</h4>
													</div>
													<div class="modal-body" id="popup-info">
														<div class="alert alert-info alert-dismissible" role="alert">
															Data HMIF berhasil disunting
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- END POPUP BERHASIL SUNTING -->
										<!-- POPUP HAPUS -->
										<div class="modal fade" id="hapus<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document" id="modal-small">
												<div class="modal-content">
													<button style="float:right; background: red; color : white;" type="button" class="close-button" data-dismiss="modal">
														<i class="fa fa-times"></i>
													</button>
													<div class="modal-header">
														<h4 class="modal-title" id="exampleModalLabel">
															<i class="fa fa-trash"></i> Hapus Data
														</h4>
													</div>
													<div class="modal-body" id="popup-hapus">
														<p class="text-center">Yakin ingin menghapus data ini?</p>
														<div class="text-center">
															<!-- data-toggle="modal" data-target="#berhasil-hapus" data-dismiss="modal" -->
															<a href="query/hapusDataHMIF.php?id=<?php echo $data['id']; ?>" type="button" class="btn btn-primary" style="margin-right: 20px">Ya</a>
															<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- END POPUP HAPUS -->
											</tr>
									<?php }
											} ?>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END BASIC TABLE -->
		</div>
		<!-- END MAIN -->

<?php include('admin-component/adm-footer.php') ?>
