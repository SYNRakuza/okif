	<!-- Footer section -->
	<footer class="footer-section">
		<div class="container">
			
			<ul class="main-menu footer-menu">
				<li><a href="index.php">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>
			<div class="footer-social d-flex justify-content-center footer-content">
				<a href="https://www.facebook.com/okifftuh/" style="font-size: 2rem"><i class="fa fa-facebook"></i></a>
				<a href="https://twitter.com/okifftuh/" style="font-size: 2rem"><i class="fa fa-twitter"></i></a>
				<a href="https://www.instagram.com/okifftuh/" style="font-size: 2rem"><i class="fa fa-instagram"></i></a>
				<a href="https://www.youtube.com/okifftuh/" style="font-size: 2rem"><i class="fa fa-youtube"></i></a>
			</div>
			<div class="copyright"><a href="">OKIF FT-UH</a> 2019 @ All rights reserved</div>
		</div>
	</footer>
	<!-- Footer section end -->


	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.sticky-sidebar.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/main.js"></script>

	</body>
</html>
