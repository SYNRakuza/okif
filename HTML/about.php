<?php include('component/header.php'); ?>


	<!-- Page top section -->
	<section class="page-top-section-about set-bg" data-setbg="img/background2.png">
		<div class="page-info">
			<h2>About Us</h2>
			<div class="site-breadcrumb">
				<a href="">Home</a>  /
				<span>About</span>
			</div>
		</div>
	</section>
	<!-- Page top end-->

	<!-- Intro section -->
	<section class="intro-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="intro-text-box text-box text-white">
						<h2 class="newH2"><span style="font-size: 10vw;">WELCOME!</span></h2>
						<p>Organisasi Kemahasiswaan Informatika Fakultas Teknik Universitas Hasanuddin ialah Organisasi Kemahasiswaan di Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin yang kemudian disingkat OKIF FT-UH</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	<!-- aboutt -->
	<section class="review-section" style="align: center;">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="intro-text-box text-box text-white">
						<h2 class="newH2"><span style="font-size: 10vw;">SEJARAH SINGKAT</span></h2>
						<p align="justify">Sejarah Singkat OKIF FT-UH Teknik Informatika dan Teknik Elektro merupakan sebuah program studi yang berada di dalam naungan Jurusan Teknik Elektro Fakultas Teknik Universitas Hasanuddin. Pada Awalnya, lembaga kemahasiswaan untuk Mahasiswa Informatika masih diwadahi oleh OKJE FT-UH (Organisasi Kemahasiswaan Jurusan Elektro Fakultas Teknik Universitas Hasanuddin). Namun, karena telah beredarnya SK Dekan Fakultas Teknik Universitas Hasanuddin Nomor:3758/UN.4.8/KM.12/2015 tentang pengelolahan dan Penyelenggaraan Kemahasiswaan di Fakultas Teknik Universitas Hasanuddin, dimana pelaksanaan kelembagaan mahasiswa sudah tidak lagi berbasis jurusan, melainkan berbasis program studi. Oleh karena itu, seluruh program studi merasa perlu melaksanakan musyawarah untuk menindaklanjuti hal tersebut, tak terkecuali Mahasiswa Teknik Informatika, dimana Mahasiswa Teknik Informatika pada saat itu merasa perlu untuk melakukan musyawarahnya sendiri. Musyawarah ini kemudian diberi nama dengan Musyawarah Mahasiswa Informatika Fakultas Teknik Universitas Hasanuddin (MMIF FT-UH). Pada saat itu MMIF FT-UH baru pertama kali dilakukan, maka jadilah hal tersebut MMIF I FT-UH. Kepengurusan pun berlangsung dan melahirkan sayembara Logo OKIF FT-UH dan sayembara PDH OKIF FT-UH yang selanjutnya menghasilkan Logo OKIF FT-UH dan PDH OKIF FT-UH.</p>
						<p align="justify">Setelah Kepengurusan berlangsung, akhirnya dibukalah MMIF II FT-UH pada tanggal 09 April 2016, dimana Badan Pekerja MMIF II FT-UH dikoordinir oleh kanda Muh. Fariqussalam Malik (T'IF 12). Akhirnya pada tanggal 18 April 2016 Pukul 01:44 WITA bertempat di Bontomarannu, Gowa, Kanda Muh. Sahas Awaluddin dinyatakan Laporan Pertanggungjawabannya diterima dan dinyatakan demisioner sebagai Ketua Umum OKIF FT-UH pada saat itu. Banyak Pembicaraan yang terjadi di dalam MMIF II FT-UH hingga pada saatnya melahirkan sebuah mufakat bahwa OKIF FT-UH kekuasaanya akan dibagi menjadi dua, yaitu HMIF FT-UH dan DMMIF FT-UH. Hal ini tentunya didasari oleh surat ketetapan nomor 006/TAP/MMIF FT-UH/V/2016 tentang Peninjauan Kembali Pedoman Dasar Organisasi Kemahasiswaan Informatika Fakultas Teknik Universitas Hasanuddin (DMMIF FT-UH) merupakan lembaga legislatif dan yudikatif pada OKIF FT-UH yang senantiasa berkoordinasi dengan Badan Perwakilan Mahasiswa Fakultas Teknik Universitas Hasanuddin (BPM FT-UH). Sedangkan Himpunan Mahasiswa Informatika Fakultas Teknik Universitas Hasanuddin (HMIF FT-UH) merupakan lembaga eksekutif pada OKIF FT-UH yang senantiasa berkoordinasi dengan Senat Mahasiswa Fakultas Teknik Universitas Hasanuddin (SMFT-UH). Penjelasan ini sesuai dengan lampiran Surat Ketetapan Nomor 006/TAP/MMIF FT-UH/V/2016 tentang Peninjauan Kembali Pedoman Dasar Organisasi Kemahasiswaan Informatika Fakultas Teknik Universitas Hasanuddin pada pasal 27 dan pasal 38.</p>
						<!-- ?php include 'koneksi.php'  ?>
						<?php 
							$data = mysqli_query($koneksi,"SELECT * FROM sejarah");
							while($get = mysqli_fetch_array($data)){
						?>
						<p><?= $get['sejarah']?></p>
						<?php } ?> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Intro section -->
	<section class="intro-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="intro-text-box text-box text-white">
						<h2 class="newH2"><span style="font-size: 10vw;">TUJUAN DAN USAHA</span></h2>
						<h4>Tujuan</h4>
						<p align="justify">OKIF FT-UH bertujuan sebagai sarana peningkatan kualitas ketakwaan kepada Tuhan Yang Maha Esa, yang berasaskan nilai-nilai luhur pancasila demi terwujudnya cita-cita bangsa dengan mengembangkan wawasan, integritas dan potensi diri, kemampuan keprofesian, serta pengabdian kepada masyarakat.
						</p>
						<h4>Usaha</h4>
						<p align="left">OKIF FT-UH  berusaha : <br/>
						a.	Memberi bimbingan dan motivasi kepada Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin untuk senantiasa bertakwa kepada Tuhan Yang Maha Esa serta menjadi unsur pemimpin dan penggerak di segala bidang  kehidupan bangsa, utamanya  dalam bidang ilmu yang digelutinya; <br/>
						b.	Menghimpun dan menggerakkan Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin dalam membina kesadaran beragama, berilmu, bermahasiswa, dan bermasyarakat;<br/>
						c.	Meningkatkan kemampuan berpikir dan daya nalar Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin;<br/>
						d.	Menciptakan suasana yang dapat meningkatkan kemampuan intelektual;<br/>
						e.	Menggiatkan kegiatan-kegiatan penelitian ilmiah pada lingkup Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin;<br/>
						f.	Menjalin hubungan yang harmonis antara Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin dengan Alumni;<br/>
						g.	Meningkatkan hubungan baik dan kerjasama dengan lembaga-lembaga intra Universitas Hasanuddin dan ekstra Universitas Hasanuddin;<br/>
						h.	Memaksimalkan link and match antara Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin dengan dunia kerja;
						dan<br/>
						i.	Menanamkan rasa cinta almamater.
						</p>
						<!-- <?php include 'koneksi.php'  ?>
						<?php 
							$data = mysqli_query($koneksi,"SELECT * FROM tujuan");
							while($get = mysqli_fetch_array($data)){
						?>
						<p><?= $get['tujuan']?></p>
						<?php } ?> -->
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- Intro section end -->

	<!-- aboutt -->
	<section class="review-section" style="align: center;">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="intro-text-box text-box text-white">
						<h2 class="newH2"><span style="font-size: 10vw;">FUNGSI DAN WEWENANG</span></h2>
						<h5>Fungsi</h5>
						<p align="left">OKIF FT-UH berfungsi: <br/>
						a.	Menampung dan menyalurkan aspirasi Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin;<br/>
						b.	Sebagai wahana Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin, yang bergerak dalam pengembangan kerohanian, penalaran dan keilmuan, minat dan bakat, kesejahteraan, profesionalisme, dan  pengabdian pada masyarakat; serta <br/>
						c.	Mengoordinasikan dan melaksanakan kegiatan intrakurikuler dan ekstrakurikuler organisasi kemahasiswaan di Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin.<br/>
						</p>
						<h5>Wewenang</h5>
						<p align="left">OKIF FT-UH mempunyai wewenang:<br/>
						a.	Memberi saran dan pandangan dalam rangka pencapaian tujuan pendidikan nasional, serta keterangan mengenai pelaksanaan hak dan kewajiban mahasiswa kepada pihak yang terkait;<br/>
						b.	Merumuskan dan menetapkan penjabaran aturan-aturan OKIF FT-UH;<br/>
						c.	Memberi sanksi kepada mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin dan/atau pengurus yang melanggar aturan OKIF FT-UH;<br/>
						d.	Melakukan upaya advokasi terhadap Mahasiswa Departemen Teknik Informatika Fakultas Teknik Universitas Hasanuddin kepada pihak yang terkait; dan<br/>
						e.	Mengeluarkan segala kebijakan yang berhubungan dengan tujuan, usaha, dan fungsi OKIF FT-UH.

							
						</p>
						<!-- <?php include 'koneksi.php'  ?>
						<?php 
							$data = mysqli_query($koneksi,"SELECT * FROM fungsi");
							while($get = mysqli_fetch_array($data)){
						?>
						<p><?= $get['fungsi']?></p>

						<?php } ?> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Intro section -->
	<!-- <section class="intro-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="intro-text-box text-box text-white">
						<h2 class="newH2">FUNGSI DAN WEWENANG</h2>
						<?php include 'koneksi.php'  ?>
						<?php 
							$data = mysqli_query($koneksi,"SELECT * FROM fungsi");
							while($get = mysqli_fetch_array($data)){
						?>
						<p><?= $get['fungsi']?></p>
						<?php } ?>
					</div>
				</div>
				
			</div>
		</div>
	</section> -->
	<!-- Intro section end -->

	<?php include('component/footer.php'); ?>