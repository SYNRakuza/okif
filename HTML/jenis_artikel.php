<?php include('component/header.php'); ?>

<?php
function limit_words($string, $word_limit){
$words = explode(" ",$string);
return implode(" ", array_splice($words,0,$word_limit));
}
?>

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="img/page-top-bg/6.jpg">
	<div class="page-info">
		<h2>Blog</h2>
		<div class="site-breadcrumb">
			<a href="">Home</a> /
			<span>Blog</span>
		</div>
	</div>
</section>
<!-- Page top end-->


<!-- Blog page -->
<section class="blog-page">
	<div class="container">
		<div class="row">
			<ul class="blog-filter">
                <li><a href="jenis_artikel.php?kategori=info%20kegiatan">Info Kegiatan</a></li>
				<li><a href="jenis_artikel.php?kategori=info%20beasiswa">Info Beasiswa</a></li>
				<li><a href="jenis_artikel.php?kategori=prestasi">Prestasi</a></li>
				<li><a href="jenis_artikel.php?kategori=info%20kemahasiswaan">Info Kemahasiswaan</a></li>
			</ul>
			<div class="col-xl-9 col-lg-8 col-md-7">
				<?php include 'koneksi.php'  ?>
                <?php
                    $kategori=$_GET['kategori'];
					$paginasi = true;
					$batas=3;
					if (isset($_GET['halaman'])) {
						$halaman = $_GET['halaman'];
					}
					if(empty($halaman)){
					$posisi  = 0;
					$halaman = 1;
					}
					else{
						$posisi  = ($halaman-1) * $batas;
					}
				$data = mysqli_query($koneksi, "SELECT * FROM artikel WHERE kategori='$kategori' LIMIT $posisi,$batas");
				while ($get = mysqli_fetch_array($data)) {
					$waktu = date('d M Y', strtotime($get['tanggal']));
					?>
					<div class="big-blog-item">
						<?= "<img src='admin/image/" . $get['foto'] . "'style='width:640px; height:360px;'>" ?>
						<div class="blog-content text-box text-white">
							<div class="top-meta"><?= $get['tanggal'] ?> / in <a><?= $get['kategori'] ?></a></div>
							<h3><?= $get['judul'] ?></h3>
							<?php
							$long_string = $get['isi'];
							$limited_string = limit_words($long_string, 50);
							echo $limited_string."...";
							?>
							<a href="readmore.php?id=<?php echo $get['id']; ?>" class="read-more">Read More <img src="img/icons/double-arrow.png" alt="readmore.php" /></a>
						</div>
					</div>
				<?php } ?>

				<div class="site-pagination">
					
						<!-- Langkah 3: Hitung total data dan halaman serta link 1,2,3  -->
						<?php
						
						if ($paginasi) {
							$data2 = mysqli_query($koneksi, "SELECT * FROM artikel WHERE kategori='$kategori'");
							$jmldata    = mysqli_num_rows($data2);
							$jmlhalaman = ceil($jmldata/$batas);

							$previous = $halaman - 1;
							for($i=1;$i<=$jmlhalaman;$i++) {
								if ($i != $halaman){
									echo " <a href=\"jenis_artikel.php?kategori=$kategori&halaman=$i\">$i</a>";
								} else {
									echo "<a class=\"active\">$i</a>";
								}
							}
							$next = $halaman + 1;
						}
						?>
					
				</div>
			</div>
			<div class="col-xl-3 col-lg-4 col-md-5 sidebar">
				<div id="stickySidebar">
					<div class="widget-item">
						<div class="categories-widget">
							<h4 class="widget-title">kategori</h4>
							<ul>
								<li><a href="jenis_artikel.php?kategori=info%20kegiatan">Info Kegiatan</a></li>
								<li><a href="jenis_artikel.php?kategori=info%20beasiswa"">Info Beasiswa</a></li>
								<li><a href="jenis_artikel.php?kategori=prestasi">Prestasi</a></li>
								<li><a href="jenis_artikel.php?kategori=info%20kemahasiswaan">Informasi Kemahasiswaan</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('component/footer.php'); ?> -->