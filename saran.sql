-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2019 at 01:19 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `okif`
--

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

CREATE TABLE `saran` (
  `id_saran` int(5) NOT NULL,
  `saran_nama` varchar(50) NOT NULL,
  `saran_email` varchar(50) NOT NULL,
  `saran_perihal` varchar(200) NOT NULL,
  `saran_isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id_saran`, `saran_nama`, `saran_email`, `saran_perihal`, `saran_isi`) VALUES
(1, 'Hermawan Safrin', 'var_email', 'var_perihal', 'Lorem Ipsum Dolor Sit Amet'),
(2, 'Gilbert', 'var_email', 'var_perihal', 'dlasdklas;dksal;dkasl;dkas asd;aslk das;kd ;lasdk ;asldk l;asdksa;ldasl; dkas;ldk sa;'),
(3, 'Jumel', 'jumel@gmail.com', 'var_perihal', 'askldsakldasd aslkdnasld asdlk asdl asdlk sadsa dlkas dlkas daskl dlask daskld askldasdklas dsa dkasdmaslkdmas kldmas kldmas kdlasm dsa mdklasm dlk asmd adl aksmdasl kdmadl ma dm al'),
(4, 'Sanji', 'Sanji09@gmail.com', 'asdas', 'ekasmdasklmdasklmdkaslmdkl'),
(5, 'Sanji', 'Sanji09@gmail.com', 'asdas', 'ekasmdasklmdasklmdkaslmdkl'),
(6, 'Sanji', 'Sanji09@gmail.com', 'asdas', 'ekasmdasklmdasklmdkaslmdkl'),
(7, 'dsad', 'dasdas@gmail.cmo', 'sadas@gmail.com', 'kasmdasdsamd.,mas.,dmas.,dmas,dmas.,dmas.dmsd asdm as,dmasdasndmsand');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id_saran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id_saran` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
